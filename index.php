<?php

//require('animal.php');
require('Frog.php');
require('Ape.php');


$sheep = new Animal("shaun");

echo "Nama Hewan :  $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki :$sheep->legs<br>"; // 2
echo "Jenis Hewan Berdarah dingin : $sheep->cold_blooded <br><br>"; // false

$sungokong = new Ape("kera sakti");
echo "Nama Hewan :  $sungokong->name <br>"; // "shaun"
echo "Jumlah Kaki :$sungokong->legs<br>"; // 2
echo "Jenis Hewan Berdarah dingin : $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"

echo "<br><br>";

$kodok = new Frog("buduk");
echo "Nama Hewan :  $kodok->name <br>";
echo "Jumlah Kaki :$kodok->legs<br>";
echo "Jenis Hewan Berdarah dingin : $kodok->cold_blooded <br>"; // false
 $kodok->jump() ; // "hop hop"
 
?>